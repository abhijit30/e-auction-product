package com.eauction.seller.productapp.entity;

import lombok.Data;

@Data
public class ProductDetail {
    private String productId;
    private String productName;
    private String shortDescription;
    private String detailedDescription;
    private String category;
    private long startingPrice;
    private String bidEndDate;
    private String firstName;
    private String lastName;
    private String address;
    private String city;
    private String state;
    private String pin;
    private long phone;
    private String email;
}