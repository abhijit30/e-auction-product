package com.eauction.seller.productapp.dto;

import lombok.Data;

import javax.validation.constraints.*;

@Data
public class ProductDetailsDTO {
    private String productId;
    @NotNull
    @Size(min=5, max=30, message="Product name length should be between 5 to 30 characters")
    private String productName;
    private String shortDescription;
    private String detailedDescription;
    private String category;
    @NotNull
    @PositiveOrZero
    private long startingPrice;
    private String bidEndDate;
    @NotNull
    @Size(min=5, max=30, message="First Name length should be between 5 to 30 characters")
    private String firstName;
    @NotNull
    @Size(min=3, max=30, message="Last Name length should be between 3 to 30 characters")
    private String lastName;
    private String address;
    private String city;
    private String state;
    private String pin;
    @NotNull
    @Digits(integer = 10, fraction = 0)
    private long phone;
    @NotNull
    @Email
    private String email;
}