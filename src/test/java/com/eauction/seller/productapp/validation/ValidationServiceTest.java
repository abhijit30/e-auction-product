package com.eauction.seller.productapp.validation;

import com.eauction.seller.productapp.dto.ProductDetailsDTO;
import com.eauction.seller.productapp.exception.InvalidProductInputsException;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

@ExtendWith(MockitoExtension.class)
class ValidationServiceTest {
    @InjectMocks
    private ValidationService validationService;

    @Test
    void testValidateProductInputs() {
        ProductDetailsDTO productDetailsDTO = new ProductDetailsDTO();
        productDetailsDTO.setCategory("Painting");
        productDetailsDTO.setBidEndDate("12/12/2022");
        validationService.validateProductInputs(productDetailsDTO);
    }

    @Test
    void testValidateProductInputsInvalidCategory() {
        ProductDetailsDTO productDetailsDTO = new ProductDetailsDTO();
        productDetailsDTO.setCategory("Car");
        productDetailsDTO.setBidEndDate("12/12/2022");
        Exception exception = assertThrows(InvalidProductInputsException.class, () -> validationService.validateProductInputs(productDetailsDTO));
        assertEquals("com.eauction.seller.productapp.exception.InvalidProductInputsException",
                exception.getClass().getName());
        assertEquals("Category value is invalid. ", exception.getMessage());
    }

    @Test
    void testValidateProductInputsInvalidBidEndDate() {
        ProductDetailsDTO productDetailsDTO = new ProductDetailsDTO();
        productDetailsDTO.setCategory("Painting");
        productDetailsDTO.setBidEndDate("12/12/2021");
        Exception exception = assertThrows(InvalidProductInputsException.class, () -> validationService.validateProductInputs(productDetailsDTO));
        assertEquals("com.eauction.seller.productapp.exception.InvalidProductInputsException",
                exception.getClass().getName());
        assertEquals("Bid End date should be a future date.", exception.getMessage());

    }

    @Test
    void testValidateProductInvalidCategoryAndEndDate() {
        ProductDetailsDTO productDetailsDTO = new ProductDetailsDTO();
        productDetailsDTO.setCategory("Car");
        productDetailsDTO.setBidEndDate("12/12/2021");
        Exception exception = assertThrows(InvalidProductInputsException.class, () -> validationService.validateProductInputs(productDetailsDTO));
        assertEquals("com.eauction.seller.productapp.exception.InvalidProductInputsException",
                exception.getClass().getName());
        assertEquals("Category value is invalid. Bid End date should be a future date.", exception.getMessage());
    }


}